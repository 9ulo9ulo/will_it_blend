#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__prog__ = 'reman.py'
__version__ = '0.3.3'
__author__ = '9ulo9ulo'




import argparse
import logging
import os
import subprocess
import shutil
import sys
import xml.etree.ElementTree as et




class RepoManagerGit(object):


    log_levels = {'WARNING': logging.WARNING, 'INFO': logging.INFO, 'DEBUG': logging.DEBUG, 'NOTSET': logging.NOTSET}


    def __init__(self, debug_level):
        self.repo_list = []
        self.debug_level = debug_level
        self._create_logger()


    def _create_logger(self):
        #create logger
        self.logger = logging.getLogger('{}'.format(self.__class__.__name__))
        self.logger.setLevel(self.log_levels[self.debug_level])
        #create console handler and set level to debug
        console_handler = logging.StreamHandler()
        console_handler.setLevel(self.log_levels[self.debug_level])
        #create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s.%(funcName)s - %(levelname)s - %(message)s')
        #add formatter to ch
        console_handler.setFormatter(formatter)
        #add ch to logger
        self.logger.addHandler(console_handler)
        self.logger.debug('self.logger = {}'.format(self.logger))


    def _add_new_item(self, project, description, repo_url, dest_dir):
        self.repo_list.append((project, description, repo_url, dest_dir))
        self.logger.debug('added: ("{}" "{}" "{}" "{}")'.format(project, description, repo_url, dest_dir))


    def download_all(self):
        for r in self.repo_list:
            cmd_list = ['git', 'clone', '-v', r[2], r[3]]
            self.logger.debug('cmd_list = {}'.format(cmd_list))
            p = subprocess.Popen(cmd_list, shell=False)
            #std_out, std_err = p.communicate()
            p.communicate()
            ret_code = p.returncode
            if ret_code:
                self.logger.error('ret_code = {}'.format(ret_code))
                sys.exit(ret_code)


    def update_all(self):
        for r in self.repo_list:
            cwd = os.getcwd()
            self.logger.debug('cwd = {}'.format(cwd))
            dest_dir = r[3]
            if os.path.exists(dest_dir) and os.path.isdir(dest_dir):
                os.chdir(dest_dir)
                self.logger.debug('switching dir to: "{}"'.format(os.getcwd()))
                cmd_list = ['git', 'pull', '-v']
                self.logger.debug('cmd_list = {}'.format(cmd_list))
                p = subprocess.Popen(cmd_list, shell=False)
                #std_out, std_err = p.communicate()
                p.communicate()
                ret_code = p.returncode
                if ret_code:
                    self.logger.error('ret_code = {}'.format(ret_code))
                    sys.exit(ret_code)
                os.chdir(cwd)
            else:
                self.logger.warning('could not find dir "{}"'.format(dest_dir))
                self.logger.warning('skipping')


    def delete_all(self):
        cwd = os.getcwd()
        self.logger.debug('cwd = {}'.format(cwd))
        for r in self.repo_list:
            dest_dir = r[3]
            self.logger.info('deleting dir "{}" please wait...'.format(dest_dir))
            if os.path.exists(dest_dir) and os.path.isdir(dest_dir):
                shutil.rmtree(dest_dir)
                self.logger.info('dir "{}" successfully removed'.format(dest_dir))
            else:
                self.logger.warning('could not find dir "{}"'.format(dest_dir))
                self.logger.warning('skipping')




class RepositoryManager(object):


    log_levels = {'WARNING': logging.WARNING, 'INFO': logging.INFO, 'DEBUG': logging.DEBUG, 'NOTSET': logging.NOTSET}


    def __init__(self, **kwargs):
        self.debug_level = kwargs['debug_level']
        self.xml_file_path = kwargs['xml_file_path']
        self.download = kwargs['download']
        self.update = kwargs['update']
        self.renew = kwargs['renew']
        self.repo_list = []
        self._create_logger()
        self._check_params()
        self.reman_git = RepoManagerGit(debug_level=self.debug_level)
        #add other repository managers here if needed
        self._parse_xml_file()


    def _create_logger(self):
        #create logger
        self.logger = logging.getLogger('{}'.format(self.__class__.__name__))
        self.logger.setLevel(self.log_levels[self.debug_level])
        #create console handler and set level to debug
        console_handler = logging.StreamHandler()
        console_handler.setLevel(self.log_levels[self.debug_level])
        #create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s.%(funcName)s - %(levelname)s - %(message)s')
        #add formatter to ch
        console_handler.setFormatter(formatter)
        #add ch to logger
        self.logger.addHandler(console_handler)
        self.logger.debug('self.logger = {}'.format(self.logger))


    def _check_params(self):
        self.logger.debug('debug_level = {}'.format(self.debug_level))
        self.logger.debug('xml_file_path = {}'.format(self.xml_file_path))
        self.logger.debug('download = {}'.format(self.download))
        self.logger.debug('update = {}'.format(self.update))
        self.logger.debug('renew = {}'.format(self.renew))
        self.logger.debug('repo_list = {}'.format(self.repo_list))


    def _parse_xml_file(self):
        tree = et.parse(self.xml_file_path)
        tree_root = tree.getroot()
        for r in tree_root.findall('repo'):
            vcs = r.find('vcs').text
            if vcs == 'git':
                self.reman_git._add_new_item(project=r.find('project').text,
                                             description=r.find('description').text,
                                             repo_url=r.find('repo_url').text,
                                             dest_dir=r.find('dest_dir').text)
            else:
                self.logger.error('ERROR! unsupported vcs: {}'.format(vcs))
                sys.exit(1)
        for r in self.repo_list:
            self.logger.debug('r = {}'.format(r))


    def download_all(self):
        self.logger.info('STARTED: downloading, please wait...')
        self.reman_git.download_all()
        self.logger.info('DONE: downloading finished')


    def update_all(self):
        self.logger.info(msg='STARTED: updating, please wait...')
        self.reman_git.update_all()
        self.logger.info('DONE: updating finished')


    def delete_all(self):
        self.logger.info('STARTED: deleting, please wait...')
        self.reman_git.delete_all()
        self.logger.info('DONE: deleting finished')


    def renew_all(self):
        self.logger.info('STARTED: renewing, please wait...')
        self.reman_git.delete_all()
        self.reman_git.download_all()
        self.logger.info('DONE: renewing finished')




#main function
def main(cli_args_dict):
    rm = RepositoryManager(**cli_args_dict)
    if cli_args_dict['download']:
        rm.download_all()
    if cli_args_dict['update']:
        rm.update_all()
    if cli_args_dict['clean']:
        rm.delete_all()
    if cli_args_dict['renew']:
        rm.renew_all()




#call context
if __name__ == '__main__':
    cli_parser = argparse.ArgumentParser(prog=__prog__,
                                         description=open('descr.txt').read())
    cli_parser.add_argument('-c',
                            '--config',
                            dest='xml_file_path',
                            metavar='XML_FILE_PATH',
                            default='repos.xml',
                            help='full path to *.xml repos configuration file (default: %(default)s)',)
    me_group = cli_parser.add_mutually_exclusive_group()
    me_group.add_argument('-d',
                          '--download',
                          dest='download',
                          action='store_true',
                          help='download (clone) all repositories from *.xml config file (default: %(default)s)')
    me_group.add_argument('-u',
                          '--update',
                          dest='update',
                          action='store_true',
                          help='update (pull) all repositories from *.xml config file (default: %(default)s)')
    me_group.add_argument('--clean',
                          dest='clean',
                          action='store_true',
                          help='clean all local repositories based on *.xml config file (default: %(default)s)')
    me_group.add_argument('--renew',
                          dest='renew',
                          action='store_true',
                          help='renew all repositories from *.xml config file (default: %(default)s)')
    cli_parser.add_argument('--debug',
                            dest='debug_level',
                            choices=['DEBUG', 'INFO', 'WARNING', 'NOTSET'],
                            default='INFO',
                            help='increasing debug level (default: %(default)s)')
    cli_parser.add_argument('--version',
                            action='version',
                            version='%(prog)s '+__version__)
    cli_args = cli_parser.parse_args()
    cli_args_dict=vars(cli_args)
    if not cli_args_dict['download'] and not cli_args_dict['update'] and not cli_args_dict['clean'] and not cli_args_dict['renew']:
        cli_parser.print_help()
    else:
        main(cli_args_dict)




#70D0:
# - add missing docstrings
# - validate *.xml config file against xml schema
# - try to handle .gitignore file as well
# - log file could be implemented as well
# - new option for examine working copy status could be added too

